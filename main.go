package main

import (
	"fmt"
	"gitlab.com/yjagdale/autoscaler-tester/app"
)

func main() {
	fmt.Println("Starting application")
	app.StartApplication()
}