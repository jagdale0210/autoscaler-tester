package app

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
	"time"
)

func StartApplication() {
	fmt.Println("Starting application server on :8080")
	r := gin.Default()
	r.GET("/ping", Ping)
	r.POST("/load", load)
	r.POST("/load/:durationToRun", load)
	err := r.Run()
	if err != nil {
		fmt.Println("Error while starting service" + err.Error())
		os.Exit(1)
	}
}

func Ping(c *gin.Context) {
	c.JSON(200, gin.H{
		"message": "pong",
	})
}

func load(c *gin.Context) {

	duration, err := strconv.Atoi(c.Param("durationToRun"))

	if err != nil {
		duration = 5
	}

	endTime := time.Now().Add(time.Duration(duration) * time.Minute)
	var counter = 0
	for time.Now().Before(endTime) {
		for i := 0; i <= counter; i++ {
			var data [][]byte
			go func() {
				contents, err := ioutil.ReadFile("test.file")
				if err != nil {
					fmt.Printf("Error while reading file %s\n", err.Error())
					return
				}
				data = append(data, contents)
			}()
		}
		counter = counter + 1000
		time.Sleep(1 * time.Second)
	}
	fmt.Printf("Final Count %d\n", counter)
	c.JSON(http.StatusOK, gin.H{"status": "execution Done"})
	return
}