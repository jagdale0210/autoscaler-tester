FROM golang:1.16.0 as builder

WORKDIR /home/work

ADD . .
RUN go build -o application main.go
RUN chmod 777 application

FROM golang:1.16.0

WORKDIR /home/app

COPY --from=builder /home/work/application application
COPY --from=builder /home/work/test.file test.file

EXPOSE 8080

ENTRYPOINT ["/home/app/application"]
